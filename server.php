<?php
session_start();

$nume = "";
$prenume = "";
$telefon = "";
$nrTelefon = "";
$username = "";
$email1    = "";
$email    = "";
$currentPassword    = "";
$newPassword    = "";
$confirmPassword    = "";

$errors = array(); 


require_once "config.php";
$con = new mysqli($DBservername, $DBusername, $DBpassword, $DBname);
if (!$con) die ("Connection failed: ". $con -> connect_error);
	echo "Connected successfully";
	echo "<br>";

// REGISTER USER
if (isset($_POST['reg_user'])) {
	if (isset($_POST['gen']))
		$gen = $_POST['gen'];
	if (isset($_POST['stareCivila']))
		$stareCivila = $_POST['stareCivila'];
  $nume = mysqli_real_escape_string($con, $_POST['nume']);
  $prenume = mysqli_real_escape_string($con, $_POST['prenume']);
  $regex = "[+][0-9]{2}[' '][1-9]{2,5}[' '][1-9]*?";
  $telefon = mysqli_real_escape_string($con, $_POST['telefon']);
  $nrTelefon = preg_match($regex, $telefon);
  $username = mysqli_real_escape_string($con, $_POST['username']);
  $email1 = mysqli_real_escape_string($con, $_POST['email']);
  $email = filter_var($email1, FILTER_VALIDATE_EMAIL);
  $parola1 = mysqli_real_escape_string($con, $_POST['parola1']);
  $parola2 = mysqli_real_escape_string($con, $_POST['parola2']);

  // upload image
  $target_dir = "uploads/";
  $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
  $uploadOk = 1;
  $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
  $target_file = $target_dir . $username . "." . $imageFileType;
  echo $target_file;

	$check = getimagesize ($_FILES["fileToUpload"]["tmp_name"]);
	if ($check !== false) {
		echo "File is an image - " .$check["mime"].".";
		$uploadOk = 1;
	}else{
		 array_push($errors, "Exista deja inregistrat acest email.");
}

if (file_exists ($target_file)) {
	array_push($errors, "Sorry, file already exists.");
	
}

if ($_FILES["fileToUpload"]["size"] > 500000) {
	array_push($errors, "Sorry, your file is too large.");
}
if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
	 array_push($errors, "Sorry, only JPG, JPEG, PNG, GIF files are allowed.");
}

  
 // array push pt erori
	if (empty($gen)) { array_push($errors, "Selecteaza sexul."); }
	 if (empty($stareCivila)) { array_push($errors, "Selecteaza starea civila."); }
  if (empty($nume)) { array_push($errors, "Numele este necesar."); }
  if (empty($prenume)) { array_push($errors, "Prenumele este necesar."); }
   if (empty($telefon)) { array_push($errors, "Numarul de telefon este necesar."); }
  if (empty($username)) { array_push($errors, "Username este necesar."); }
  if (empty($email1)) { array_push($errors, "Email este necesar."); }
  if (empty($parola1)) { array_push($errors, "Parola este necesara."); }
  if ($parola1 != $parola2) {
	array_push($errors, "Cele doua parola nu corespund.");
  }

  // daca exista deja user si parola
  $user_check_query = "SELECT * FROM users WHERE username='$username' OR email='$email' LIMIT 1";
  $result = mysqli_query($con, $user_check_query);
  $user = mysqli_fetch_assoc($result);
  
  if ($user) { // daca user exista
    if ($user['username'] === $username) {
      array_push($errors, "Exista deja un user inregistrat cu acest username.");
    }
	
    if ($user['email'] === $email) {
      array_push($errors, "Exista deja inregistrat acest email.");
    }
  }

  // Inregistrare user daca nu sunt erori de completare
  if (count($errors) == 0) {
  	$password = md5($parola1);//criptare
// salvare data cand s-a inregistrat
$date = date('Y-m-d H:i:s');
// salvare extensie
	$extension = end((explode(".", $target_file)));
	echo $extension;
  	$query = "INSERT INTO Users (gen, stareCivila, nume, prenume, telefon, username, email, parola1, dateinregistrare, extension) 
  			  VALUES('$gen', '$stareCivila', '$nume', '$prenume', '$telefon', '$username', '$email', '$password', '$date', '$extension')";
  	mysqli_query($con, $query);
	
	// salvare foto in bd
	if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
			echo "The file has been uploaded.";
		}else{
			echo "Sorry, there was an error uploading your file.";
		}
  	$_SESSION['username'] = $username;
  	$_SESSION['success'] = "Esti logat.";
	$_SESSION['password'] = $password;
  	header('location: index.php');
  }
}

// LOGIN USER
if (isset($_POST['login_user'])) {
  $username = mysqli_real_escape_string($con, $_POST['username']);
  $password = mysqli_real_escape_string($con, $_POST['password']);

  if (empty($username)) {
  	array_push($errors, "Username este necesar.");
  }
  if (empty($password)) {
  	array_push($errors, "Parola este necesara.");
  }

  if (count($errors) == 0) {
  	$password = md5($password);
  	$query = "SELECT * FROM users WHERE username='$username' AND parola1='$password'";
  	$results = mysqli_query($con, $query);
  	if (mysqli_num_rows($results) == 1) {
  	  $_SESSION['username'] = $username;
	   $_SESSION['password'] = $password;
  	  $_SESSION['success'] = "Esti logat";
  	  header('location: index.php');
  	}else {
  		array_push($errors, "Username si/sau parola gresite.");
  	}
  }
}


// CHANGE PASSWORD
if (isset($_POST['change_password'])) {
	$currentPassword    = md5 (mysqli_real_escape_string($con, $_POST['currentPassword']));
	$newPassword    = md5 (mysqli_real_escape_string($con, $_POST['newPassword']));
	$confirmPassword    = md5 (mysqli_real_escape_string($con, $_POST['confirmPassword']));
 
  if (empty($currentPassword) || empty($newPassword) || empty($confirmPassword )) {
  	array_push($errors, "Campul este necesar.");
  }
  if ($_SESSION['password'] != $currentPassword) array_push($errors, "Parola curenta este gresita.");
  
   if ($newPassword != $confirmPassword) array_push($errors, "Cele doua parola noi cu corespund.");
   
   if ($currentPassword == $confirmPassword) array_push($errors, "Parola noua trebuie sa fie diferita de cea veche.");
	
	if (count($errors) == 0) {
		mysqli_query($con, "UPDATE users set parola1='" . $newPassword . "' WHERE username='" . $_SESSION["username"] . "'");
		
			$message = "Parola a fost schimbata cu succes";
	} else
		
			$message = "Eroare in salvarea noii parole.";
	
}



?>