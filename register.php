<?php include('server.php') ?>
<!DOCTYPE html>
<html>
<head>
  <title>Registration</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="header">
  	<h2>Register</h2>
  </div>
	
	
  <form method="post" action="register.php"  enctype="multipart/form-data">
  
   Selecteaza o imagine: </br>
    <input type="file" name="fileToUpload" id="fileToUpload"></br>
	
  	<?php include('errors.php'); ?>
	<div class="input-group">
	<label>Gen<br></label>
		<input type="radio" name="gen" value="masculin"> Masculin<br>
		<input type="radio" name="gen" value="feminin"> Feminin<br>
	</div>
	<div class="input-group">
	<label>Stare civila<br></label>
		<input type="radio" name="stareCivila" value="casatorit"> Casatorit<br>
		<input type="radio" name="stareCivila" value="necasatorit"> Necasatorit<br>
	</div>
	<div class="input-group">
  	  <label>Nume</label>
  	  <input type="text" name="nume" value="<?php echo $nume; ?>">
  	</div>
  	<div class="input-group">
  	  <label>Prenume</label>
  	  <input type="text" name="prenume" value="<?php echo $prenume; ?>">
  	</div>
	<div class="input-group">
  	  <label>Telefon</label>
  	  <input type="text" name="telefon" value="<?php echo $telefon; ?>">
  	</div>
  	  <label>Username</label>
  	  <input type="text" name="username" value="<?php echo $username; ?>">
  	</div>
  	<div class="input-group">
  	  <label>Email</label>
  	  <input type="email" name="email" value="<?php echo $email; ?>">
  	</div>
  	<div class="input-group">
  	  <label>Parola</label>
  	  <input type="password" name="parola1">
  	</div>
  	<div class="input-group">
  	  <label>Confirma parola</label>
  	  <input type="password" name="parola2">
  	</div>
  	<div class="input-group">
  	  <button type="submit" class="btn" name="reg_user">Inregistreaza-te</button>
  	</div>
  	<p>
  		Deja membru? <a href="login.php">Sign in</a>
  	</p>
  </form>
 
</body>
</html>